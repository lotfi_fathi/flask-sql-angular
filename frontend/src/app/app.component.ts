import { Component, OnInit } from '@angular/core';
import { ApiService } from './api.service';

interface User {
  id: number;
  nom: string;
  prenom: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'demoApp';

  users: User[] = [];

  constructor(private apiService: ApiService){}

  ngOnInit(): void {
    this.apiService.listUsers().subscribe(
      res => {
        console.log(res.data);
        this.users = res.data;
      }
    )
  }

}
