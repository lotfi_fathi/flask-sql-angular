import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  baseUrl:string = 'http://127.0.0.1:5000/';

  constructor(private httpClient: HttpClient) { }

  listUsers() {
    return this.httpClient.get<any>(this.baseUrl + 'users');
  }
}
