import os
import logging
from config import Config
from flask import Flask, jsonify, request
from flask_cors import CORS
from database import User, db

SECRET_KEY = os.environ.get('SECRET_KEY')
SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI')
SQLALCHEMY_TRACK_MODIFICATIONS = False
    
    
app = Flask(__name__)
app.config.from_object(Config)
db.init_app(app)
CORS(app)

@app.route('/')
def hello():
    return 'Hello World'

@app.route('/users')
def getUsers():
    users = User.query.all()
    data = []
    for user in users:
        data.append({
            'id': user.id,
            'nom': user.nom,
            'prenom': user.prenom,
        })
    return jsonify({'data': data}), 200

@app.route('/users/create')
def create_user():
    nom = request.json['nom']
    prenom = request.json['prenom']
    user = User(nom=nom, prenom=prenom)
    db.session.add(user)
    db.session.commit()
    return jsonify({
        'message': 'User Created',
        'project': {
            'id': user.id,
            'nom': user.nom,
            'prenom': user.prenom,
        }
    }), 200

@app.before_first_request
def create_table():
    db.create_all()
